import hashlib
import zipfile
import re
import glob, os
from lxml import html
import time
import random
import requests
from proxies import proxies
import config



def randomize_request_proxies(renewed_proxy=''):
    """
    Set a random number to choose a proxy.
    If there is renewed proxy (because of blocked IP), there would be need to
    create new random number.
    """
    if renewed_proxy:
        random_number = renewed_proxy
    else:
        get_random_number()
        random_number = first_random_number

    proxy = proxies[random_number]
    print(f"PROXY NUMBER {random_number}")
    request_proxy = {'http':f'http://{config.PROXY_USERNAME}:{config.PROXY_PASS}@{proxy}',
                    'https':f'http://{config.PROXY_USERNAME}:{config.PROXY_PASS}@{proxy}'}
    return request_proxy


def parse_response_xpath(response,xpath,random_number=''):
    print(response.status_code)
    new_ip = first_random_number
    while response.status_code >= 400:
        print(f"IP {new_ip} is Blocked, retry request in another IP")
        if int(new_ip) == int(len(proxies) - 1):
            new_ip = 0
        else:
            new_ip += 1
        
        if new_ip == first_random_number:
            print("ALL PROXIES BLOCKED")
            break
        response = init_request(response.url,proxies=randomize_request_proxies(new_ip))

    tree = html.fromstring(html=response.text)
    return tree.xpath(xpath)
    
def randomize_selenium_proxies():
    random_number = get_random_number()
    print(f"PROXY NUMBER {random_number}")
    return proxies[random_number]

def get_random_number():
    """
    Creates a first random number that will be set as a Global Variable
    To be used on different functions as reference
    """
    global first_random_number
    first_random_number = random.randrange(0,len(proxies))
    ###add static random number for testing purposes of proxies
    # first_random_number = 5
    return first_random_number


def init_request(url,add_header='',proxies=randomize_request_proxies()):
    retries = 0
    while retries < 6:
        retries += 1
        time.sleep(1)
        try:
            response = requests.get(
                url = url,
                headers=parse_headers(add_header),
                proxies=proxies,
                allow_redirects=True,
                timeout = 10
            )
            time.sleep(8)
            return response

        except requests.ConnectionError as e:
            print("OOPS!! Connection Error. Make sure you are connected to Internet. Technical Details given below.\n")
            print(str(e))            
            # renewIPadress()
            continue
        except requests.Timeout as e:
            print("OOPS!! Timeout Error")
            continue
        except requests.RequestException as e:
            print("OOPS!! General Error")
            print(str(e))
            continue
        except KeyboardInterrupt:
            print("Someone closed the program")

        

    # raise Exception("Maximum retries exceeded")


def get_store_function(modules_location,merchant_id):

    if 'stores' not in os.getcwd():
        os.chdir("./stores")
    for file in glob.glob("*.py"):
        lines = open(file, 'r').readlines()
        for line in lines:
            if 'merchant_id' in line:
                file_id = re.search("\d+", line)

                if int(merchant_id) ==  int(file_id[0]):
                    module = f"{modules_location}.{(file.replace('.py',''))}"
                    return module
                break


def parse_headers(add_header=''):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36',
        'accept-language': 'en-US,en;q=0.9',
    }
    if add_header:
        for key,value in add_header.items():
            headers[key]=value
    return headers


def get_digits_only(price):
    '''
    Cleans string type variables. If it is a list, transform it to a string by
    calling its index.
    '''
    try:
        price = re.findall("\d+\D\d+", price)[0]
        price = float(price.replace(",","."))
    except:
        price = None

    return price
























def generate_extension(proxy, credentials):
    ip, port = proxy.split(':')
    login, password = credentials.split(':')
    manifest_json = """
    {
        "version": "1.0.0",
        "manifest_version": 2,
        "name": "Chrome Proxy",
        "permissions": [
            "proxy",
            "tabs",
            "unlimitedStorage",
            "storage",
            "",
            "webRequest",
            "webRequestBlocking"
        ],
        "background": {
            "scripts": ["background.js"]
        },
        "minimum_chrome_version":"22.0.0"
    }
    """

    background_js = """
    var config = {
            mode: "fixed_servers",
            rules: {
            singleProxy: {
                scheme: "http",
                host: "%s",
                port: parseInt(%s)
            },
            bypassList: ["localhost"]
            }
        };

    chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

    function callbackFn(details) {
        return {
            authCredentials: {
                username: "%s",
                password: "%s"
            }
        };
    }

    chrome.webRequest.onAuthRequired.addListener(
                callbackFn,
                {urls: [""]},
                ['blocking']
    );
    """ % (ip, port, login, password)

    sha1 = hashlib.sha1()
    sha1.update(("%s:%s" % (proxy, credentials)).encode('utf-8'))
    filename = sha1.hexdigest() + ".zip"

    with zipfile.ZipFile(filename, 'w') as zp:
        zp.writestr("manifest.json", manifest_json)
        zp.writestr("background.js", background_js)

    return filename