import importlib
import json
import os
import random
import sys
import time
from concurrent.futures import Future, ProcessPoolExecutor, ThreadPoolExecutor

import requests

import config
from aks_utils import clean_url, skip_stores, stores_dont_clean_url
from proxies import proxies
from utils import (get_random_number, get_store_function, init_request,
                   parse_response_xpath)

CONCURRENT_REQUEST = 2
toplinks_start_point = 0
prod_links_start_point = 0

modules_location = "stores"
starting_url = 'https://www.allkeyshop.com/blog/'


class AllkeyShop():

    def __init__(self, starting_url):
        self.starting_url = starting_url
        self.top_link_counter = 0
        self.start_requests()

    def start_requests(self):
        links_xpath = '*//div[@id="Top25"]/a/@href'
        response = init_request(self.starting_url)
        self.top_links = parse_response_xpath(response, links_xpath)

        top_link_counter = 0
        self.get_game_data()

    def get_game_data(self):
        for top_link in self.top_links[toplinks_start_point:]:
            self.top_link = top_link
            print(f'Getting ID of {self.top_link}')
            self.top_link_counter += toplinks_start_point
            game_id_xpath = '//div[contains(@class,"metacritic-button")]/@data-product-id'
            response = init_request(top_link)
            self.game_id = parse_response_xpath(response, game_id_xpath)[0]

            self.parse_data(self.get_aks_data())
            self.top_link_counter += 1

    
    def parse_data(self, data):
        with ProcessPoolExecutor(max_workers=CONCURRENT_REQUEST) as executor:
            for counter, data in enumerate(data[prod_links_start_point:]):
                executor.submit(self.create_async_request,counter,data)


    def create_async_request(self,counter,data):
        self.merchant_id = data['merchant']
        self.buy_url_tier = data['buy_url_tier']
        self.edition = data['edition']
        self.store_url = clean_url(self.merchant_id, data['buy_url'])
        
        self.counter = counter
        self.sent_message_already = False
        self.counter += prod_links_start_point
        self.aks_price = float(data['price'])

        if self.merchant_id in skip_stores:
            print(f'Skipping this store {self.merchant_id}')
            return

        
        self.parse_price()

    def parse_price(self):
        
        price = None
        message = ''
        store_function = get_store_function(modules_location, self.merchant_id)
        try:
            module = importlib.import_module(store_function)
        except:
            mesage = "NO FUNCTION YET"
            content = f"{self.top_link_counter}-{self.counter} Merchant: {self.merchant_id} {message}"
            content += f"\n{self.top_link}"
            content += f"\n\t{self.aks_price}"
            content += f"\n{self.store_url}"
            sent = self.send_discord_msg(
                content, config.discord_lacking_function)

        try:
            price = module.get_price(url=self.store_url, 
                                    url_tier=self.buy_url_tier, 
                                    edition=self.edition)
        except:
            message = 'Cannot Find Price'
            # self.send_discord_msg(message)

        self.price = price
        self.create_terminal_message(message)

        return self.price

    def randomize_selenium_proxies():
        random_number = get_random_number()
        print(f"PROXY NUMBER {random_number}")
        return proxies[random_number]

    def create_terminal_message(self, message=''):
        discord_endpoint = config.discord_bot_problem
        if self.price is not None:
            difference = self.aks_price - self.price
            if difference != 0:
                if abs(difference) > .1:
                    message = "**PRICE ERROR**"
                    discord_endpoint = config.discord_price_problem
                else:
                    message = "**SMALL DIFFERENCE**"
                    discord_endpoint = config.discord_bot_small_difference

            else:
                message = "Price OK"

        
        content = f"{self.top_link_counter} - {self.counter} {message} merchant id:{self.merchant_id}"
        content += f"\n{self.top_link}"
        content += f"\n\t{self.aks_price}"
        content += f"\n{self.store_url}"
        content += f"\n\t{self.price}"
        print("___________________________________________________________________________________________________________")
        print(content)
        if message != 'Price OK':
            self.send_discord_msg(content, discord_endpoint)

    def create_discord_message(message=''):
        pass

    def send_discord_msg(self, content, discord_endpoint):
        if self.sent_message_already == False:
            payload = {"content": content, }
            headers = {'authorization': config.DISCORD_AUTH}
            r = requests.post(discord_endpoint, data=payload, headers=headers)
            if r:
                self.sent_message_already = True

        return False

    def get_aks_data(self):
        api = f"https://www.allkeyshop.com/admin/bot_de_v2/marc_api.php?game_id={self.game_id}&version={time.time()}"
        r = requests.get(api, auth=(config.user, config.password))
        return json.loads(r.text)




if __name__ == '__main__':
    aks = AllkeyShop(starting_url)
