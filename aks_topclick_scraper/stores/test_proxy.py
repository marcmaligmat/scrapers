from utils import init_request, parse_response_xpath, get_digits_only

merchant_id = 1000

def get_price(**kwargs):
    url = kwargs['url']

    response = init_request(url)
    xpath = '//span[@id="ipv4"]/a/text()'

    try:
        result = parse_response_xpath(response,xpath)
        print(result)
    except:
        pass