from utils import init_request, parse_response_xpath, get_digits_only

merchant_id = 162

def get_price(**kwargs):
    url = f"{kwargs['url']}?currency=EUR"
    xpath = '//div[@class="price"]/text()'
    response = init_request(url)
    price = parse_response_xpath(response, xpath)[0]
    price = get_digits_only(price)
    return price