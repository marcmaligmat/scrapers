from utils import init_request, parse_response_xpath, get_digits_only

merchant_id = 9

def get_price(**kwargs):
    url = kwargs['url']
    xpath = '//div[@class="product-info-price"]//span[contains(@id,"product-price")]/@data-price-amount'
    response = init_request(url)
    price = parse_response_xpath(response, xpath)[0]
    price = get_digits_only(price)
    return price

