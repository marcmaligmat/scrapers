from utils import init_request, parse_response_xpath, get_digits_only

merchant_id = 272


def get_price(**kwargs):
    url = kwargs['url']
    xpath = '(//span[@class="_1fTsyE"]/span[@class="_3RZkEb"]/text())[1]'
    xpath2 = '(//span[@class="_1fTsyE"]/span[@class="_3RZkEb"]/text())[2]'
    response = init_request(url)

    price = get_digits_only(parse_response_xpath(response, xpath)[0])
    try:
        price2 = get_digits_only(parse_response_xpath(response, xpath2)[0])
        if price2:
            if price2 < price:
                price = price2
    except:
        pass

    
        
    return price
