from utils import init_request, parse_response_xpath, get_digits_only

merchant_id = 94


def get_price(**kwargs):
    url = kwargs['url']
    response = init_request(url)
    xpath = '//div[@class="price_list"]/div[@class="price"]/@content'
    price = parse_response_xpath(response, xpath)[0]

    return get_digits_only(price)
