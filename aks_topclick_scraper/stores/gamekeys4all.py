from utils import init_request, get_digits_only, parse_response_xpath
import json
import re

merchant_id = 31

def get_price(**kwargs):
    url = kwargs['url']
    response = init_request(url)
    xpath = '//span[@id="our_price_display"]/text()'
    price = parse_response_xpath(response, xpath)[0]
    return get_digits_only(price)
