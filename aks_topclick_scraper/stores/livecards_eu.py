from utils import init_request, parse_response_xpath, get_digits_only

merchant_id = 318

def get_price(**kwargs):
    url = kwargs['url']
    xpath = '(//div[@class="price"]/p[@class="game-price"]/span[@class="the-price"]/text())[2]'
    response = init_request(url)
    price = parse_response_xpath(response, xpath)[0]
    price = get_digits_only(price)

    return price
