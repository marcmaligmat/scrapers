from utils import init_request, get_digits_only
import json
import re

merchant_id = 218


def get_price(**kwargs):
    url = kwargs['url']
    url = re.findall(r'[^\/]+$',url)[0]
    url = f"https://www.gamivo.com/api/product/product-by-slug/{url}"

    response = init_request(url)
    print(response.status_code)
    data = json.loads(response.text)['offers'][0]
    price = data['0']['price']

    try:
        data2 = json.loads(response.text)['offers'][1]
        price2 = data2['0']['price']
        if price2 and price > price2:
            price = price2
    except:
        print("No Second Price")
    # print(price)

    return price
