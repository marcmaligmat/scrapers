from utils import init_request, parse_response_xpath, get_digits_only

merchant_id = 7

def get_price(**kwargs):
    url = kwargs['url']
    xpath = '//span[contains(text(),"Disponibilit")]/preceding-sibling::span/span[@class="price_current"]/text()'
    response = init_request(url)
    price = parse_response_xpath(response, xpath)[0]
    price = get_digits_only(price)
    return price