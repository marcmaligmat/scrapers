from utils import ( init_request, parse_response_xpath, 
                    get_digits_only, randomize_selenium_proxies)
from my_driver import driver_wait_xpath, run_driver
import time
import re

merchant_id = 61


def get_price(**kwargs):
    url = kwargs['url']
    driver = run_driver(randomize_selenium_proxies, False)

    base_url = 'https://www.g2a.com/en/'
    splitted_url = str.split(url,'/')
    url = f"{base_url}{splitted_url[-1]}"
    # url = re.sub(r"g2a.com\/[^\/]+",'g2a.com/en',url)
    print(url)
    driver.get(url)
    
    time.sleep(4)
    xpath = '//span[@class="sc-igOljT eZYAZY"]'
    xpath2 = '(//span[@class="sc-hmbsMR frTSYm"])[1]'

    price = driver_wait_xpath(driver,xpath).text
    try:
        price2 = driver.find_element_by_xpath(xpath2).text
        if price2 and float(price2) < float(price):
            price = price2
    except:
        print("cannot see second price")

    

    time.sleep(4)
    driver.close()
    driver.quit()
    time.sleep(1)
    return get_digits_only(price)
