from utils import init_request, parse_response_xpath, get_digits_only
from urllib.parse import urljoin

merchant_id = 122


def get_price(**kwargs):
    url = kwargs['url']
    url = urljoin(url, '?currency=EUR')
    response = init_request(url)
    xpath = '//p[@class="special-price"]/span[@class="price"]/text()'

    price = parse_response_xpath(response, xpath)[0]

    return get_digits_only(price)
