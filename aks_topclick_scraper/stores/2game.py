from utils import init_request, parse_response_xpath, get_digits_only

merchant_id = 12

def get_price(**kwargs):
    url = kwargs['url']
    xpath = '(//div[@class="details-info-left"]//span[@class="price"]/text())[1]'
    xpath2 = '(//div[@class="details-info-left"]//span[@class="price"]/text())[2]'
    response = init_request(url)

    try:
        price = parse_response_xpath(response, xpath2)[0]
    except:
        price = parse_response_xpath(response, xpath)[0]
    price = get_digits_only(price)
    return price