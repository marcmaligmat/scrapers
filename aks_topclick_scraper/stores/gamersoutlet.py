from utils import init_request, parse_response_xpath, get_digits_only

merchant_id = 77

def get_price(**kwargs):
    url = kwargs['url']
    xpath = '//div[@class="product__info__prices__wrapper"]//h2/text()'
    response = init_request(url)
    price = parse_response_xpath(response, xpath)[0]
    price = get_digits_only(price)
    return price