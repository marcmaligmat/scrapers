from utils import init_request, get_digits_only, parse_response_xpath
import json
import re

merchant_id = 310

def get_price(**kwargs):
    url = kwargs['url']
    response = init_request(url)
    new_url = parse_response_xpath(response, '//a/@href')
    new_url = f"https://www.gamesload.com{new_url[0]}"

    response = init_request(new_url)
    xpath = '(//div[@class="buy-block"]//p[@class="price-current"]/text())[1]'
    price = parse_response_xpath(response, xpath)[0]

    return get_digits_only(price)
