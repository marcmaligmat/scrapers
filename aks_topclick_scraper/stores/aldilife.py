from utils import init_request, parse_response_xpath, get_digits_only

merchant_id = 305

def get_price(**kwargs):
    url = kwargs['url']
    xpath = '(//div[@class="product-details__price-subline"]/preceding-sibling::div[1]/text())[2]'
    response = init_request(url)
    price = parse_response_xpath(response, xpath)[0]
    price = get_digits_only(price)
    return price