from utils import init_request, parse_response_xpath, get_digits_only

merchant_id = 701

def get_price(**kwargs):
    url = kwargs['url']
    xpath = '//span[contains(text(),"Availa")]/preceding-sibling::span/span[@class="price_current"]/text()'
    response = init_request(url)
    price = parse_response_xpath(response, xpath)[0]
    price = get_digits_only(price)
    price *= 1.1578
    return price