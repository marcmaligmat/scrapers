import re

from utils import init_request, parse_response_xpath, get_digits_only

merchant_id = 67

def get_price(**kwargs):
    url = kwargs['url']
    url_tier = kwargs['url_tier']
    url = f"{url}?setCurrencyId=3"
    response = init_request(url)
    results = re.findall(r"price:\s\'\D(\d+\D\d+)\'\,\s+sku\:\s+\'([^\,]*)\'", response.text)
    if len(results) > 0:
        if len(results) > 1:
            for res in results:
                res = list(res)
                if url_tier == res[1]:
                    price = res[0]
                    break
        else:
            price = list(results[0])[0]
        return float(price)
    else:
        xpath = '//em[@class="ProductPrice VariationProductPrice"]/text()'
        try:
            result = parse_response_xpath(response,xpath)
            price = get_digits_only(result[1])
        except:
            price = get_digits_only(result[0])

        return price

    