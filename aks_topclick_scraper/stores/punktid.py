from utils import init_request, parse_response_xpath, get_digits_only

merchant_id = 307

def get_price(**kwargs):
    url = kwargs['url']
    xpath = '//div[contains(@class,"field-item ")]/text()'
    response = init_request(url)
    price = parse_response_xpath(response, xpath)[0]
    price = get_digits_only(price)

    return price
